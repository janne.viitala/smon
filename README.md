# Smon - Real-time data visualizer using html and WebGL

## Introduction
Smon - Signal monitor - is real-time visualizer for numerical signal data.
It is intended to use e.g. industrial control system as data source, but this is
not provided by this demo.

Smon provides number of visualization widgets, such as plain number display, graph plot and text enumeration.

Smon starts in edit mode where user can build screens with desired signals visualized, 
and then switch to run mode with [>] button to start visualizing. 
In Run-mode it is also possible to write value to signals.

Edit mode provides undo/redo, copy/paste and other typical GUI editing functions.

Smon in edit mode, with enumeration widget being selected:
![smon edit](smon1.png)

Smon in run mode, with the same display configuration:
![smon edit](smon2.png)

Display configuration saving and loading not implemented yet.


## Technologies
Smon uses Vue.js and Vuetify for the UI, and WebGL (with fallback to canvas) via Pixi.js library for rendering the visualized content. 

Electron build included for building stand-alone desktop application (not tested recently).

Some demonstratory unit tests using jest.


## To run:

``` 
$ cd smon
$ npm install 
$ npm run serve
or
$ npm run electron:serve
```

## Author
Janne Viitala 
[janne.viitala@gmail.com]()

## License
Proprietary

