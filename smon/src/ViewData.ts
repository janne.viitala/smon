/**  Data to be edited in d'n'd' & property editors, and rendered by graphics classes 
 *   It's all grahics backend agnostic here.
 */

import Vue from 'vue';
import * as _ from 'lodash';
import { ItemData, IGraphicsObject } from './ItemData';

const MAX_HISTORY = 100;  // undo history

export class Point {
  public x: number;
  public y: number;
  public constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}

// Someone (it's PEditArea) needs to create and destroy IGraphicsObject when new items are created/deleted,
// redraw the whole scene, etc.
export interface IGraphicsEngine {
  create: (data: ItemData, type: string, dataExists: boolean) => IGraphicsObject;  // create graphics object
  draw: () => void;             // repaint all
  drawDecorations: () => void;  // re-draw/hide grid & borders
  map2Local: (screenX: number, screenY: number) => Point;
  run: (ena: boolean) => void;  // run mode on/off
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * all data in the view area, with undo/redo and some generic arranging functions
 * 
 */
export class ViewData {
  public id = 0;  // for testing undo
  public items: Array<ItemData> = []; 
  public historyIndex = -1;
  public historyLength = 0;
  public canUndo = false;
  public canRedo = false;
  
  private graphicsEngine: IGraphicsEngine | null = null;
  private clipBoard: Array<ItemData> = []; 
  private history: Array<Array<ItemData>> = [];
  private static _instance: ViewData;
  private isRunning = false; 

  public constructor() {
    console.log("viewData created");  // must see only one; this is singleton
  }

  public static instance(): ViewData {
    if (!this._instance) {
      this._instance = new this();
    }
    return this._instance;
  }

  // PEditArea registers here
  public installPObjectFactory(f: IGraphicsEngine): void {   
    this.graphicsEngine = f;
  }

  // run mode get & set
  public get running(): boolean {
    return this.isRunning;
  }
  public set running(run: boolean) {
    this.isRunning = run;
    this.graphicsEngine?.run(this.isRunning);
    this.items.forEach((i) => { 
      i.graphics?.select(false); 
      i.graphics?.toggleRunMode(run);
    });
  }

  // called by graphics engine when it's time to draw signal values
  public runModeUpdateAll(): void {
    this.items.forEach((item) => { item.graphics?.updateValues(); });
  }

  public writeToSelected(value: number): void {
    this.items.forEach((item) => {
      if (item.selected) {
        item.graphics?.writeValue(value);
      }
    })
  }

  // clone gui objects, for undo/redo etc.
  private cloneItems(source: Array<ItemData>): Array<ItemData> {
    const clonedItems: Array<ItemData> = [];
    source.forEach(item => {
      clonedItems.push(item.clone());
    });
    return clonedItems;
  }

  // re-paint everything
  public redraw(): void {
    this.graphicsEngine?.draw();
  }

  public createAt(type: string, screenX: number, screenY: number): void {
    const data = new ItemData;                  // All persistent data is in ItemData,
    if (this.graphicsEngine) {               // always true
      const localPos = this.graphicsEngine.map2Local(screenX, screenY);
      data.setx(localPos.x);
      data.sety(localPos.y);
      data.type = type;
      this.items.push(data);                      // and we store it in 'items',
      this.graphicsEngine?.create(data, type, false);  // and this fellar knows how to create matching Pixi graphics object.
      this.redraw();
      this.pushUndoIfChanged();
    }
  }

  // create new item
  public create(type: string): void {
    const data = new ItemData;                  // All persistent data is in ItemData,
    data.type = type;
    this.items.push(data);                      // and we store it in 'items',
    this.graphicsEngine?.create(data, type, false);  // and this fellar knows how to create matching Pixi graphics object.
  }

  public count(): number {
    return this.items.length;
  }

  public deleteSelected(): void {
    _.remove(this.items, i => {
      if (i.selected) {
        console.log("deleting...");
        i.graphics?.delete();  // remove from screen
        // Note: ItemData's in the history still point to anove pobj, it will not be garbage
        //       collected until all of them have dropped away from history.
        return true;       // tells _.remove() to remove this ItemData
      }
      return false;
    });
    this.pushUndoIfChanged();
    this.redraw();
  }

  public cut(): void {
    this.clipBoard = [];  // throw away the old content
    _.remove(this.items, i => {
      if (i.selected) {
        i.graphics?.delete();  // remove from screen
        this.clipBoard.push(i);
        return true;       // tells _.remove() to remove this ItemData
      }
      return false;
    });
    this.pushUndoIfChanged();
    this.redraw();
  }

  public copy(): void {
    this.clipBoard = [];  // throw away the old content
    this.items.forEach(i => {      
      if (i.selected) {
        const copiedObject = i.clone();
        copiedObject.attachPObject(null);  
        this.clipBoard.push(copiedObject);
      }
    });
  }

  public paste(): void {
    this.clipBoard.forEach(cpItem => {
      const newItem: ItemData = cpItem.clone();
      // TODO: now pastes to same position from where was copied, add some sanity here
      if (this.graphicsEngine) { // always true...
        newItem.attachPObject(this.graphicsEngine.create(newItem, newItem.type, true));
      }
      newItem.graphics?.setData(newItem);
      this.items.push(newItem);
    });
    this.redraw();
  }

  public numSelected(): number {
    let cnt = 0;
    this.items.forEach(i => { 
      if (i.selected) {
        cnt++; 
      }
    });
    return cnt;
  }
  

  // push current state to undo stack for later undo.
  // return: true if pushed
  public pushUndoIfChanged(): boolean {
    // check if something has really changed and it's thus worth pushing it
    if (this.history[this.historyIndex] && 
      this.items.length === this.history[this.historyIndex].length) {
      let same = true;
      for (let ix=0; ix<this.items.length; ix++) {
          if (!this.items[ix].isEqual(this.history[this.historyIndex][ix])) {
            same = false;
            break;
          }
      }
      if (same) { 
        //console.log("state not changed, not pushing to undo stack");  // never here :-(
        return false;  
      }    
    }

    if (this.historyIndex < (this.history.length - 1)) {    // pushing after undoing; throw away undoed past
      //console.log(`throwing away ${this.history.length - this.historyIndex + 1} last actions`);
      this.history = this.history.slice(0, this.historyIndex + 1);
    }
    else if (this.history.length >= MAX_HISTORY) {          // buffer is full, drop oldest
      this.history.shift();
    }
    this.items.forEach(i => { i.historyId = this.id });
    this.id ++;  // to separate different instances
    const copy = this.cloneItems(this.items); 
    this.history.push(copy);
    this.historyIndex = this.history.length - 1;
    this.historyLength = this.history.length;
    this.canUndo = (this.historyIndex > 0);
    this.canRedo = false;
    return true;
  }

  public undo = (): void => {
    if (this.canUndo && this.historyIndex >= 0) {
      //console.log(`undo'ing from index ${this.historyIndex-1}, current id=${this.id}`);
      this.historyIndex--;
      // this is for undo'ing cut: need to hide graphics objects to make sure no "ghosts" remain
      this.items.forEach(item => { item.graphics?.delete(); });
      this.items = this.cloneItems(this.history[this.historyIndex]);
      this.items.forEach(i => {
        i.graphics?.setData(i);
      });  // make PObject's point to this new data      
      this.graphicsEngine?.draw();
      this.canRedo = true;
      if (this.historyIndex == 0)
        this.canUndo = false;
    } 
    else {
      //console.log(`can't undo any more, index=${this.historyIndex}, length=${this.history.length}`);
    }
  }


  public redo = (): void => {
    if (this.canRedo && this.historyIndex < (this.history.length - 1)) {
      this.historyIndex++;
      //console.log("redo'ing from index " + this.historyIndex.toString());
      // this is for redo'ing delete: 1st delete all graphics objects...
      this.items.forEach(item => { item.graphics?.delete(); });
      // and then replace our objects with ones from the history...
      this.items = this.cloneItems(this.history[this.historyIndex]);
      // and then re-enable graphics objects that still exists.
      this.items.forEach(i => { i.graphics?.setData(i); });  // make PObject's point to this new data
      this.graphicsEngine?.draw();
      this.canUndo = true;
      if (this.historyIndex === (this.history.length - 1)) {
        this.canRedo = false;
      }
    }
    else {
      console.log(`can't redo any more, index=${this.historyIndex}, length=${this.history.length}`);
      this.canRedo = false;
    }
  }

  // raise/lower: bigger z is up
  public selectedToFront(): void {
    this.items.forEach(i => { 
      if (i.selected) {
        i.setz(i.z() + 999999);
      }
    });
    this.normalizeZs();
    this.items.forEach(i => { 
      i.graphics?.update();
    });
    this.pushUndoIfChanged(); // TODO: filter out cases when nothing happens
  }

  public selectedToBack(): void {
    this.items.forEach(i => { 
      if (i.selected) {
        i.setz(i.z() - 999999);
      }
    });  
    this.normalizeZs();
    this.items.forEach(i => { 
      i.graphics?.update();
    });
    this.pushUndoIfChanged(); // TODO: filter out cases when nothing happens
  }

  public largestZIndex(): number {
    let zMax = -99999;
    this.items.forEach(i => { 
      if (i.z() > zMax)
        zMax = i.z();
    });
    return zMax;
  }

  // make z's continuous from 1
  private normalizeZs(): void {
    const newZs: Array<number> = [];
    this.items.forEach(i => { 
      let z = 0;
      this.items.forEach(ii => { 
        if (ii.z() <= i.z()) {
          z++;
        }
      });
      newZs.push(z);
    });  
    let ix = 0;
    this.items.forEach(i => { 
      i.setz(newZs[ix]);
      ix++;
    });

  }

  public alignSelectedToLeft(): void {
    let minX = 999999;
    // search for smallest x
    this.items.forEach(i => { 
      if (i.selected && i.x() < minX) {
        minX = i.x();
      }
    });
    // check if there is something to move
    let doIt = false;
    this.items.forEach(i => { 
      if (i.selected && i.x() != minX) {
        doIt = true;
      }
    });
    // move all to smallest x
    if (doIt) {
      this.items.forEach(i => { 
        if (i.selected) {
          i.setx(minX);
          i.graphics?.update();
        }      
      });
      this.pushUndoIfChanged();
    }
  }


  public alignSelectedToRight(): void {
    let maxX = -1;
    // search for largest x+w (right edge)
    this.items.forEach(i => { 
      if (i.selected && i.x() + i.w() > maxX) {
        maxX = i.x() + i.w();
      }
    });
    // check if there is something to move
    let doIt = false;
    this.items.forEach(i => { 
      if (i.selected && (i.x() + i.w()) != maxX) {
        doIt = true;
      }
    });
    // move all to largest x
    if (doIt) {
      this.items.forEach(i => { 
        if (i.selected) {
          i.setx(maxX - i.w());
          i.graphics?.update();
        }      
      });
      this.pushUndoIfChanged();
    }
  }


  public alignSelectedToCenter(): void {
    let minX = 999999;
    // search for smallest x+w/2 (right edge)
    this.items.forEach(i => { 
      if (i.selected && i.x() + i.w()/2 < minX) {
        minX = i.x() + i.w()/2;
      }
    });
    // check if there is something to move
    let doIt = false;
    this.items.forEach(i => { 
      if (i.selected && (i.x() + i.w()/2) != minX) {
        doIt = true;
      }
    });
    // move all 
    if (doIt) {
      console.log("doit");
      this.items.forEach(i => { 
        if (i.selected) {
          i.setx(minX - i.w()/2);
          i.graphics?.update();
        }      
      });
      this.pushUndoIfChanged();
    }
    else {
      console.log("nothing to arrange");
    }
  }


  // make nice stack out of selected objects
  public alignSelectedToStack(): void {
    let minX = 999999;
    let maxW = -1;
    let maxH = -1;
    let minY = 999999;
    this.items.forEach(i => { 
      if (i.selected) {
        // search for smallest x,y
        if (i.x() < minX) {
          minX = i.x();
        }
        if (i.y() < minY) {
          minY = i.y();
        }
        // search for largest w (right edge)
        if (i.w() > maxW) {
          maxW = i.w();
        }
        // search for largest h
        if (i.h() > maxH) {
          maxH = i.h();
        }
      }
    });
    // now move all (too lazy to check if anything is really moved, may result undo actions doing nothing)
    let cnt = 0;
    this.items.forEach(i => { 
      if (i.selected) {
        i.setx(minX);
        i.sety(minY + maxH * cnt);
        i.setw(maxW);
        i.seth(maxH);
        cnt++;
        i.graphics?.update();
      }      
    });
    this.pushUndoIfChanged();
  }
}

// singleton , as Vue reactive object
export const viewData = Vue.observable(ViewData.instance());
