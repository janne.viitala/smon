/*  Data to be rendered & edited in d'n'd' & property editors */

import * as _ from 'lodash';
import Lasso from './pixi/PLasso';

// different property types - select editor component in property panel
export enum PropertyType {
  STRING = 0,  // value is string
  SIGNAL,      // value is signal name
  NUMBER,      // value is number
  COLOR,       // value is Color class: see below
  VALIGN,      // value is number: -1=left, 0=center, 1=right
  HALIGN,      // value is number: -1=top, 0=center, 1=botton
  BOOLEAN,     // value is boolean
  ENUM,        // value is list of enums
  SELECTION    // value is SelectionData class: see below
}


// Data for both renrering & GUI property editor
export class Property {
  public name: string;
  public type: PropertyType;
  public value: any;  // string, number, color, ...
  public constructor(name: string, type: PropertyType, value: any) {  // value is some of PropertyType
    this.name = name;
    this.type = type;
    this.value = value;
  }
}


// grouping is for GUI
export class PropertyGroup {
  public name = "";
  public props: Array<Property> = [];
}


export class Color {
  public rgb: string;   // css color string
  public a: number;     // alpha, 0..1
  public constructor(rgb?: string, a?: number) {
    this.rgb = rgb && rgb || "#000000";
    this.a = a && a || 1.0;
  }
}


// one enum value/name pair for enum editor
export class Enum {
  public label: string;
  public value: number;
  public customColors: boolean;
  public backgroundColor: Color;
  public textColor: Color;
  public constructor(label: string, value: number) {
    this.value = value;
    this.label = label;
    this.customColors = false;
    this.backgroundColor = new Color("#006600", 1);
    this.textColor = new Color("#ffffff", 1);
  }
}

// property value for SELECTION, contains active value and list of options
export class SelectionData {
  public sel = "";
  public options: Array<string> =  [];
}

// interface for graphics object rendering this item (Pixi based drawing object, or similar) 
export interface IGraphicsObject {
  // edit mode stuff:
  update: () => void;                 // redraw, after properties has been changed
  select: (sel: boolean) => void;     // toggle selection markers
  setData: (data: ItemData) => void;  // set new ItemData (undo/redo)
  delete: () => void;                 // remove from screen - update() will show again
  selectIfInside: (lasso: Lasso) => void;
  resize: (corner: number, dx: number, dy: number) => void; 
  move: (dx: number, dy: number) => void;
  // run-time update:
  toggleRunMode: (run: boolean) => void;
  updateValues: () => void;
  writeValue: (value: number) => void; // user writes the value

}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * data for single item - dynamically extended by derived classes
 * 
 */
export class ItemData {

  public selected = false;  // current selection status - only for PObject!
  public historyId = 0;     // debugging

  // Our Pixi graphics object. Better call attachPObject() before doing anything (PObject does)
  // May be null in case this is history buffer ItemData that has been deleted.
  public graphics: IGraphicsObject | null = null;  
  
  public type: string; // TST downcasting...

  // editable properties:
  // Note: PObject could add these too...
  public properties: Array<PropertyGroup> = [{
    name: "Geometry",
    props: [
      {
        name: 'X',
        type: PropertyType.NUMBER,
        value: 0
      }, {
        name: 'Y',
        type: PropertyType.NUMBER,
        value: 0
      }, {
        name: 'Width',
        type: PropertyType.NUMBER,
        value: 0
      }, {
        name: 'Height',
        type: PropertyType.NUMBER,
        value: 0
      }, {
        name: 'Z-index',
        type: PropertyType.NUMBER,
        value: 0
      }        
    ]}
    // derived classes will add more
  ]

  public constructor() {
    this.type = "notset";
    this.setx(20);
    this.sety(20);
    this.setw(80);
    this.seth(30);
  }

  // rendering objects reports here
  public attachPObject(p: IGraphicsObject | null): void {
    this.graphics = p;
  }

  // cloning, for undo/redo etc
  public clone(): ItemData {
    /*
     * Cloning objects in ts/js is tricky - member functions are added to prototype, 
     * so it must be copied too, using Object.create as a helper.
     * https://www.nickang.com/how-to-clone-class-instance-javascript/
     */ 
    const newItem = Object.assign(Object.create(Object.getPrototypeOf(this)), this);  
    /*
     * But now we have only a shallow copy, i.e. this.properties points to old data,
     * so we must deep-copy it in a separate step.
     * Note: we can't just deep-copy everything, as it would clone really referred stuff, 
     * (e.g. IGraphicsObject) which we definetly don't want to do.
     */ 
    newItem.properties = _.cloneDeep(this.properties);
    /* we have a clone, hooray! */
    return newItem;
  }

  // compare properties
  public isEqual(other: ItemData): boolean {
      if (other.type === this.type) {
        return (JSON.stringify(other.properties) === JSON.stringify(this.properties))
      }
      return false;
  }

  // this is all editor needs; actual rendering is in the derived class:
  //   NOTE: There are copies of these in PObject for easier access from derived classes.
  //         These need to be here as editor and viewData need these.
  public x(): number { return this.properties[0].props[0].value; }
  public y(): number { return this.properties[0].props[1].value; }
  public w(): number { return this.properties[0].props[2].value; }
  public h(): number { return this.properties[0].props[3].value; }
  public z(): number { return this.properties[0].props[4].value; }
  public setx(v: number): void { this.properties[0].props[0].value = v; }
  public sety(v: number): void { this.properties[0].props[1].value = v; }
  public setw(v: number): void { this.properties[0].props[2].value = v; }
  public seth(v: number): void { this.properties[0].props[3].value = v; }
  public setz(v: number): void { this.properties[0].props[4].value = v; }
}
