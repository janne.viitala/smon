import RingBuffer from '../RingBuffer';

test('RingBuffer', () => {
  let rb = new RingBuffer(5);
  expect(rb.get()).toBe(undefined);
  rb.put(1);
  expect(rb.get()).toBe(1);
  expect(rb.get()).toBe(undefined);
  expect(rb.get()).toBe(undefined);
  rb.put(1);
  rb.put(2);
  rb.put(3);
  rb.put(4);
  expect(rb.peek(0)).toBe(4);
  expect(rb.peek(1)).toBe(3);
  expect(rb.peek(2)).toBe(2);
  rb.put(5);
  rb.put(6);
  rb.put(7);
  expect(rb.peek(0)).toBe(7);
  expect(rb.peek(1)).toBe(6);
  expect(rb.peek(2)).toBe(5);
  expect(rb.peek(3)).toBe(4);
  expect(rb.peek(4)).toBe(3);
  expect(rb.peek(5)).toBe(undefined);  // we overshoot the buffer
  expect(rb.get()).toBe(3);
  expect(rb.peek(0)).toBe(7);
  expect(rb.peek(1)).toBe(6);
  expect(rb.peek(2)).toBe(5);
  expect(rb.peek(3)).toBe(4);
  expect(rb.peek(4)).toBe(3);  // while this has been "removed" it still exists in the buffer
  expect(rb.peek(5)).toBe(undefined);  // we overshoot the buffer
  expect(rb.peek(999999)).toBe(undefined);
  expect(rb.peek(-999999)).toBe(undefined);
});
