/**
 *  Signal data source - values with timestamps.
 *  Not reactive, because our pixi components are not.
 **/


class DummySignal {
    public name = "";
    public value = 0;
}

export class SignalSource {
    private static _instance: SignalSource;
    private _orderedSignals: Array<DummySignal> = [];

    private constructor() {
        console.log("SignalSource created");
    }

    public static instance(): SignalSource {
        if (!this._instance) {
            this._instance = new this();
        }
        return this._instance;    
    }

    /** order signal, return index for getting value(s) */ 
    public order(signalName: string): number {
        for(let i = 0; i < this._orderedSignals.length; i++) {
            if (this._orderedSignals[i].name === signalName) { 
                console.log(`found signal '${signalName}' at index ${i}!`)
                return i;
            }
        }
        console.log(`added signal '${signalName}' to index ${this._orderedSignals.length}!`)
        this._orderedSignals.push({ name: signalName, value: 0 });
        return this._orderedSignals.length - 1;
    }

    public cancelOrder(signalName: string): void {
        for(let i = 0; i < this._orderedSignals.length; i++) {
            if (this._orderedSignals[i].name === signalName) { 
                this._orderedSignals.splice(i, 1); 
            }
        }
        this._orderedSignals.push({ name: signalName, value: 0 });
    }

    /** cancel all orders */
    public clearOrders(): void {
        console.log("clearOrders");
    }

    /** get value of order()'ed signal */
    public value(index: number): number {
        if (index >= 0 && index < this._orderedSignals.length) {
            return this._orderedSignals[index].value;
        }    
        return -1;
    }

    public write(index: number, value: number): void {
        if (index >= 0 && index < this._orderedSignals.length) {
            this._orderedSignals[index].value = value;
        }
    }

}

export const signalSource = SignalSource.instance();  // singleton4
