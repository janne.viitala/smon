/* Led  */

import * as PIXI from 'pixi.js'
import PObject from './PObject';
import AbstractLabel from './PAbstractLabel';
import { ItemData, PropertyGroup, Property, PropertyType, Color } from '../ItemData';

enum PropGroupIndex {
    LEDPROPS=1
}

// order must match push order in c'tor
enum LabelPropIndex {
    LABEL=0,
    PRESET,
    ONTEXTCOLOR,
    ONBGNDCOLOR,
    ON
}

const PRESET_RED = "Red";
const PRESET_GREEN = "Green";
const PRESET_ORANGE = "Orange";
const PRESET_BLUE = "Blue";
const PRESET_CUSTOM = "Custom";

export default class extends AbstractLabel {

    public constructor(cont: PIXI.Container, cb: (src: PObject, down: boolean, type: number) => void, data: ItemData, dataExists: boolean) {
        super(cont, cb, data, dataExists);
        
        if (!dataExists) {
            //
            // PropertyGroup "Led"
            // 
            const pg: PropertyGroup = this.data.properties[PropGroupIndex.LEDPROPS];
            pg.name = "Led";
            pg.props.push(new Property("Text", PropertyType.STRING, ""));        // Text label
            pg.props.push(new Property("Color Setup", PropertyType.SELECTION, { sel: PRESET_CUSTOM, 
                options: [PRESET_CUSTOM, PRESET_GREEN, PRESET_RED, PRESET_ORANGE, PRESET_BLUE] }));
            pg.props.push(new Property("Custom On Text Color", PropertyType.COLOR, { rgb: "#ffffff", a: 1 }));
            pg.props.push(new Property("Custom On Background",  PropertyType.COLOR, { rgb: "#ff0000", a: 1 }));
            pg.props.push(new Property("On",        PropertyType.BOOLEAN, false));        // TEST
            console.log("created LED");
            
            this.data.setw(20);
            this.data.seth(20);
            // off state colors for led
            this.setBgColorRGB('#110000');
            this.setTxtColorRGB('#626262');
            this.makeCircle();
        }
        this.drawIt();
    }

    private preset(): string { return this.data.properties[PropGroupIndex.LEDPROPS].props[LabelPropIndex.PRESET].value.sel; } 
    private onTextColor(): Color { return this.data.properties[PropGroupIndex.LEDPROPS].props[LabelPropIndex.ONTEXTCOLOR].value as Color;  }
    private onBgndColor(): Color { return this.data.properties[PropGroupIndex.LEDPROPS].props[LabelPropIndex.ONBGNDCOLOR].value as Color;  }
    private isOn(): boolean { return this.data.properties[PropGroupIndex.LEDPROPS].props[LabelPropIndex.ON].value;        }
    private text(): string { return this.data.properties[PropGroupIndex.LEDPROPS].props[LabelPropIndex.LABEL].value;}

    // we set colors based on on/off state
    protected bgColor(): Color { 
        if (this.preset() === PRESET_RED) {
            return this.isOn() ? new Color('#D73B04', 1) : new Color('#381001', 1);
        }
        else if (this.preset() === PRESET_GREEN) {
            return this.isOn() ? new Color('#58C142', 1) : new Color('#142E0F', 1);
        }
        else if (this.preset() === PRESET_BLUE) {
            return this.isOn() ? new Color('#0079CA', 1) : new Color('#001625', 1);
        }
        else if (this.preset() === PRESET_ORANGE) {
            return this.isOn() ? new Color('#E39214', 1) : new Color('#332106', 1);
        }
        return this.isOn() ? this.onBgndColor() : super.bgColor();  // custom
     }
     protected txtColor(): Color {
        if (this.preset() === PRESET_RED) {
            return this.isOn() ? new Color('#ffffff', 1) : new Color('#626262', 1);
        }
        else if (this.preset() === PRESET_GREEN) {
            return this.isOn() ? new Color('#ffffff', 1) : new Color('#626262', 1);
        }
        else if (this.preset() === PRESET_BLUE) {
            return this.isOn() ? new Color('#ffffff', 1) : new Color('#626262', 1);
        }
        else if (this.preset() === PRESET_ORANGE) {
                return this.isOn() ? new Color('#ffffff', 1) : new Color('#626262', 1);
        }
        return this.isOn() ? this.onTextColor() : super.txtColor();    // custom
     }

    protected drawIt(): void {
        this._text = this.text(); 
        super.drawIt();  // calls gElem.clear();
    }
}
