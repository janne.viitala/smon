/* Static label */

import * as PIXI from 'pixi.js'
import PObject from './PObject';
import AbstractLabel from './PAbstractLabel';
import { ItemData, PropertyGroup, Property, PropertyType } from '../ItemData';

enum PropGroupIndex {
    LABELPROPS = 1
}

// order must match push order in c'tor
enum LabelPropIndex {
    LABEL=0
}


export default class extends AbstractLabel {

    public constructor(cont: PIXI.Container, cb: (src: PObject, down: boolean, type: number) => void, data: ItemData, dataExists: boolean) {
        super(cont, cb, data, dataExists);
        console.log("creating a PLabel");
        if (!dataExists) {
            //    create our properties 
            const pg: PropertyGroup = this.data.properties[PropGroupIndex.LABELPROPS];
            pg.name = "Label";        
            pg.props.push(new Property("Text", PropertyType.STRING, "Label"));        // Text label
            // change some defaults
            this.setBgColorRGB("#343434");
            this.setTxtColorRGB("#ffffff");
        }
        this.drawIt();
    }

    private text(): string { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.LABEL].value;}

    protected drawIt(): void {
        this._text = this.text(); // copy our prop value to base class member to be rendered
        super.drawIt();          
    }
}
