/* Label base class - not to be instantiated, but to derived from */

import * as PIXI from 'pixi.js'
import PObject from './PObject';
import { ItemData, PropertyGroup, Property, PropertyType, Color } from '../ItemData';

enum PropGroupIndex {
    POBJPROPS=0,  // this is always 1st (x,y, etc...)
    EMPTY,        // *** Here (index 1) derived class shall add it's props ***
    LABELPROPS,
    ALIGNPROPS
}

// order must match push order in c'tor
enum LabelPropIndex {
    FONTSIZE=0,
    FONTFIXED,
    FONTBOLD,
    FONTITALIC,
    FONTSHADOW,
    TXTCOLOR,
    BGCOLOR,
    BORDERWIDTH,
    BORDERCOLOR,
    ANGLE,
    SHAPE,
    RADIUS
}

enum AlignPropIndex {
    HALIGN=0,
    VALIGN,
    MARGIN,
}

// shapes
const SHAPE_CIRCLE  = "Circle";
const SHAPE_ELLIPSE = "Ellipse";
const SHAPE_RECT    = "Rectangle";

export default class extends PObject {
    protected _text = "Text here";   // detived class shall set this

    private _ptext: PIXI.Text;
     // Keep track oc current props this to detect property changes in ItemData, this way
     // we minimize costly webgl object re-creation.
    private _currentTextSize: number;     
    private _currentTextColor: number;
    private _currentBold: boolean;
    private _currentItalic: boolean;
    private _currentShadow: boolean;
    private _currentFixed: boolean;    

    public constructor(cont: PIXI.Container, cb: (src: PObject, down: boolean, type: number) => void, data: ItemData, dataExists: boolean) {
        super(cont, cb, data, dataExists);
        
        this._currentTextSize = 16; 
        this._currentTextColor = 0;  // fixed below too!
        this._currentBold = false;
        this._currentItalic = false;
        this._currentShadow = false;
        this._currentFixed = false;

        // add empty property group for our derived classes
        let pg = new PropertyGroup;
        this.data.properties.push(pg);    

        /*    create our properties
         *
         *    !!! adjust getters/setters accordinly !!!
         */
        // PropertyGroup "Label"
        pg = new PropertyGroup;
        pg.name = "Text Display";        
        pg.props.push(new Property("Font Size", PropertyType.NUMBER, this._currentTextSize));
        pg.props.push(new Property("Fixed width", PropertyType.BOOLEAN, this._currentFixed));
        pg.props.push(new Property("Bold", PropertyType.BOOLEAN, this._currentBold));
        pg.props.push(new Property("Italic", PropertyType.BOOLEAN, this._currentItalic));
        pg.props.push(new Property("Shadow", PropertyType.BOOLEAN, this._currentShadow));
        pg.props.push(new Property("Text Color", PropertyType.COLOR, { rgb: "#000000", a: 1 }));  // note: matches to _currentTextColor
        pg.props.push(new Property("Background", PropertyType.COLOR, { rgb: "#00ff00", a: 1 }));
        pg.props.push(new Property("Border width", PropertyType.NUMBER, 1));
        pg.props.push(new Property("Border color", PropertyType.COLOR, { rgb: "#000000", a: 1 }));
        pg.props.push(new Property("Rotation angle (use with care)", PropertyType.NUMBER, 0));        
        pg.props.push(new Property("Shape",     PropertyType.SELECTION, { sel: SHAPE_RECT, options: [
                                   SHAPE_CIRCLE, SHAPE_ELLIPSE, SHAPE_RECT ] }));
        pg.props.push(new Property("Corner radius", PropertyType.NUMBER, 0));        
        // finalize
        this.data.properties.push(pg);    
        // PropertyGroup "Alignment"
        pg = new PropertyGroup;
        pg.name = "Text Alignment";  // -1, 0, 1
        pg.props.push(new Property("Horizontal Align", PropertyType.HALIGN, 0));
        pg.props.push(new Property("Vertical Align", PropertyType.VALIGN, 0));
        pg.props.push(new Property("Margin", PropertyType.NUMBER, 2));
        // finalize
        this.data.properties.push(pg);

        // create actual graphics stuff
        //this.createTextObject();  // calling this causes an error in idion ts compiler:  Property '_ptext' has no initializer and is not definitely assigned in the constructor
        this._ptext = new PIXI.Text(this._text, {  fontFamily : 'Arial', 
                                                   fontSize: this._currentTextSize, 
                                                   fill: this._currentTextColor, 
                                                   align: 'center', 
                                                   fontStyle: 'normal',
                                                   fontWeight: 'normal',
                                                   dropShadow: false, dropShadowDistance: 1, dropShadowColor: this._currentTextColor });
        this.gElem.addChild(this._ptext);
    }

    private createTextObject(): void {
        this._ptext = new PIXI.Text(this._text, {  fontFamily : this._currentFixed ? 'Courier' : 'Arial', 
                                                   fontSize: this._currentTextSize, 
                                                   fill: this._currentTextColor, 
                                                   align: 'center', 
                                                   fontStyle: this._currentItalic ? 'italic' : 'normal',
                                                   fontWeight: this._currentBold ? 'bolder' : 'normal',
                                                   dropShadow: this._currentShadow, dropShadowDistance: 1, dropShadowColor: this._currentTextColor });
        this.gElem.addChild(this._ptext);
    }

    // for derived classes to modify defaults
    protected setBgColor(color: Color): void { this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.BGCOLOR].value = color; }
    protected setTxtColor(color: Color): void { this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.TXTCOLOR].value = color; }
    protected setBgColorRGB(rgb: string): void { this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.BGCOLOR].value.rgb = rgb; }
    protected setTxtColorRGB(rgb: string): void { this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.TXTCOLOR].value.rgb = rgb; }
    protected setFontFixed(f: boolean): void { this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.FONTFIXED].value = f; }
    protected makeCircle(): void { this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.SHAPE].value.sel = SHAPE_CIRCLE; }

    // Protected getters for ItemData fields - derived classes are free to override these
    // As performance really matters here we prefer const indexes instead of some more maintainable but slower system.
    protected fontSize(): number { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.FONTSIZE].value; }
    protected fontBold(): boolean { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.FONTBOLD].value; }
    protected fontItalic(): boolean { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.FONTITALIC].value; }
    protected fontShadow(): boolean { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.FONTSHADOW].value; }
    protected fontFixed(): boolean { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.FONTFIXED].value; }
    protected angle(): number { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.ANGLE].value; }
    protected shape(): string { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.SHAPE].value.sel;  }
    protected radius(): number { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.RADIUS].value; }
    // color is object: { rgb: "#aabbcc", a=0.9 }
    protected bgColor(): Color { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.BGCOLOR].value as Color; }
    protected txtColor(): Color { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.TXTCOLOR].value as Color; }
    protected borderWidth(): number { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.BORDERWIDTH].value; }
    protected borderColor(): Color { return this.data.properties[PropGroupIndex.LABELPROPS].props[LabelPropIndex.BORDERCOLOR].value as Color; }    
    // -1, 0 or 1 (l,c,r)
    protected halign(): number { return this.data.properties[PropGroupIndex.ALIGNPROPS].props[AlignPropIndex.HALIGN].value; }
    // -1, 0 or 1 (top,c,bot)
    protected valign(): number { return this.data.properties[PropGroupIndex.ALIGNPROPS].props[AlignPropIndex.VALIGN].value; }
    protected margin(): number { return this.data.properties[PropGroupIndex.ALIGNPROPS].props[AlignPropIndex.MARGIN].value; }

    // just update the label's text from _text
    protected updateText(): void {
        this._ptext.text = this._text;
    }

    protected drawIt(): void {
        super.drawIt();  // calls gElem.clear();
        if (this._ptext) {  // super() ctor calls this
            this._ptext.text = this._text;
            //console.log(`re-drawing a AbstractLabel "${this._text}"`);
            // re-create font, if needed
            if (this.fontSize() !== this._currentTextSize || 
                this.fontBold() !== this._currentBold ||
                this.fontItalic() !== this._currentItalic ||
                this.fontShadow() !== this._currentShadow ||
                this.fontFixed() !== this._currentFixed ||
                PIXI.utils.string2hex(this.txtColor().rgb) != this._currentTextColor) 
            {
                //console.log("re-create text");
                this._currentFixed = this.fontFixed();
                this._currentBold = this.fontBold();
                this._currentItalic = this.fontItalic();
                this._currentShadow = this.fontShadow();
                this._currentTextSize = this.fontSize();
                this._currentTextColor = PIXI.utils.string2hex(this.txtColor().rgb);
                this.gElem.removeChild(this._ptext);
                this.createTextObject();
            }

            // box
            if (this.borderWidth() > 0) {
                this.gElem.lineStyle(this.borderWidth(), PIXI.utils.string2hex(this.borderColor().rgb), this.borderColor().a);
            }
            this.gElem.beginFill(PIXI.utils.string2hex(this.bgColor().rgb), this.bgColor().a);

            // draw shape
            if (this.shape() === SHAPE_CIRCLE) {
                this.gElem.drawCircle(this.data.w() / 2, this.data.h() / 2, this.data.w() / 2);
            }
            else if (this.shape() === SHAPE_ELLIPSE) {
                this.gElem.drawEllipse(this.data.w() / 2, this.data.h() / 2, this.data.w() / 2, this.data.h() / 2);
            }
            else {  // rect, possibly rounded
                if (this.radius() > 0)
                    this.gElem.drawRoundedRect(0,0, this.data.w(), this.data.h(), this.radius());
                else
                    this.gElem.drawRect(0,0, this.data.w(), this.data.h());
            }
    
            this.gElem.endFill();

            if (this.halign() < 0)
                this._ptext.x =  this.margin();
            else if (this.halign() > 0)
                this._ptext.x = this.data.w() - this._ptext.width - this.margin();
            else // center
                this._ptext.x = ((this.data.w() - this._ptext.width) / 2);
            if (this.valign() < 0)
                this._ptext.y = this.margin();
            else if (this.valign() > 0)    
                this._ptext.y = this.data.h() - this._ptext.height - this.margin();
            else
                this._ptext.y = ((this.data.h() - this._ptext.height) / 2);

            // rotation
            //  We can't set pivot to center, as it moves objects 0,0 to that point. This causes 
            //  un-intuitive coordinates (e.g. object left edge at the screen left edge; still
            //  w says w/2). And we can't move it temporarily, as it's send to GPU. So we just
            //  have to live with this. Oh well.
            //this.gElem.pivot.x = this.data.w()/2;
            //this.gElem.pivot.y = this.data.h()/2;
            this.gElem.angle = this.angle();
        }
    }
}
