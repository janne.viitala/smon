/*
*  class for handling pixi application and it's events
*/

import * as PIXI  from 'pixi.js'
import Lasso      from './PLasso';
import PObject    from './PObject';
import PLabel     from './PLabel';
import PNumber    from './PNumber';
import PLed       from './PLed';
import PPLot      from './PPlot';
import PEnum      from './PEnum';
import { ItemData, IGraphicsObject } from '../ItemData';
import { viewData, IGraphicsEngine, Point } from '../ViewData';
import { projectData, ProjectDataChangeListener } from '../ProjectData';

export default class implements IGraphicsEngine, ProjectDataChangeListener {
    private pixiApp: PIXI.Application; 
    private canvas: HTMLCanvasElement;
    private editAreaContainer: PIXI.Container;
    private lasso: Lasso;
    private itemIsSelected = false;
    private resizing = -1;
    private moveX0 = 0;
    private moveY0 = 0;
    private dragMoving = false;
    private zoom = 1.0;
    private border: PIXI.Graphics;
    private grid: PIXI.Graphics;

    public constructor (canvas: HTMLCanvasElement) {
        this.pixiApp = new PIXI.Application({
            view: canvas,
            backgroundColor: 0,
            antialias: true,
            forceCanvas: false,
            autoStart: false,
            resizeTo: canvas
        })

        this.canvas = canvas;
        const im = this.pixiApp.renderer.plugins.interaction;
        im.on('mousedown', this.onDragStart)  
          .on('mouseup',  this.onDragEnd)
          .on('mouseupoutside',  this.onDragEnd)
          .on('mousemove', this.onDragMove)

                
        this.pixiApp.stage.sortableChildren = true;
        this.editAreaContainer = new PIXI.Container;
        this.pixiApp.stage.addChild(this.editAreaContainer);
        this.editAreaContainer.sortableChildren = true;

        viewData.installPObjectFactory(this);
        
        // record the initial state
        viewData.pushUndoIfChanged();

        this.lasso = new Lasso(this.pixiApp.stage);
        
        this.border = new PIXI.Graphics();
        this.border.zIndex = -100;
        this.pixiApp.stage.addChild(this.border);
        this.grid = new PIXI.Graphics();
        this.grid.zIndex = -99;
        this.pixiApp.stage.addChild(this.grid);
        this.drawDecorations();

        projectData.addListener(this);

        this.pixiApp.ticker.add(() => { this.tickerFn(); });  // ticker cb, it runs in run mode

        this.draw();
    }

    // called when ticker is running
    public tickerFn(): void {
        viewData.runModeUpdateAll();
    }

    /**
     *  IGraphicsEngine
     */ 
    public create (data: ItemData, type: string, dataExists: boolean): IGraphicsObject {
        if (type === "PNumber")
            return this.createPObject(PNumber, data, dataExists);
        if (type === "PLabel")
            return this.createPObject(PLabel, data, dataExists);
        if (type === "PEnum")
            return this.createPObject(PEnum, data, dataExists);
        if (type === "PLed")
            return this.createPObject(PLed, data, dataExists);
        if (type === "PPlot")
            return this.createPObject(PPLot, data, dataExists);
        if (type === "PObject")
            return this.createPObject(PObject, data, dataExists);
        console.log(`don't know how to create unknown type ${type}, creating PObject instead(!)`);
        return this.createPObject(PObject, data, dataExists);        
    }    

    public draw(): void {
        this.pixiApp.renderer.render(this.pixiApp.stage);
    }

    // draw borders, grid, etc based on project settings
    public drawDecorations(): void {
        this.border.clear();
        if (projectData.showAreaBorder()) {
            this.border.lineStyle(1, PIXI.utils.string2hex(projectData.gridColor().rgb), 1);
            this.border.drawRect(0,0, projectData.areaWidth(), projectData.areaHeight());
        }
        this.grid.clear();        
        if (!viewData.running && projectData.showGrid()) {
            const grid = projectData.gridSize();
            const xMax = projectData.areaWidth();
            const yMax = projectData.areaHeight();
            const h = projectData.areaHeight();
            const w = projectData.areaWidth();
            this.grid.lineStyle(1, PIXI.utils.string2hex(projectData.gridColor().rgb), 1);
            for (let x = grid; x<xMax; x += grid) {
                this.grid.moveTo(x, 0);
                this.grid.lineTo(x, h);
            }
            for (let y = grid; y<yMax; y += grid) {                    
                this.grid.moveTo(0, y);
                this.grid.lineTo(w, y);
            }
        }
        this.pixiApp.renderer.backgroundColor = PIXI.utils.string2hex(projectData.bgColor().rgb);
        this.draw();
    }

    public run(ena: boolean): void {
        this.drawDecorations();   // no grid in run mode 
        if (ena) {
            console.log("running!");
            this.pixiApp.ticker.start();
            setTimeout(() => this.resize(), 500);
        }
        else {
            console.log("stopping!");
            setTimeout(() => this.pixiApp.ticker.stop(), 300);
        }
    }

    // convert screen coordinates to local ones - or at least try...
    public map2Local(screenX: number, screenY: number): Point {    
        const r = this.canvas.getBoundingClientRect();
        //console.log(`screen sz ${window.screen.width},${window.screen.height}, pos ${window.screenX},${window.screenY}`)
        //console.log(`converted ${window.screenX},${window.screenY} to ${r.x},${r.y}`)
        // TODO FIXME how in the earth this is supposed to work...
        let lx = screenX - (window.screenX + r.x) - 20;
        let ly = screenY - (window.screenY + r.y) - 120;  // nice guessing...
        //console.log(`result: ${lx},${ly}`)
        lx = this.align(lx);
        ly = this.align(ly);
        return new Point (lx, ly);
    }


    /**
     *   ProjectDataChangeListener
     */ 
    public projectDataChanged(): void {
        this.drawDecorations();
    } 


    // generic factory method to create PObject descendants - ah the beauty of typescript (and questionable design with callback)!
    private createPObject<TYPE extends PObject> (c: {new(p: PIXI.Container, cb: (src: PObject, down: boolean, type: number) => void, data: ItemData, dataExists: boolean): TYPE}, data: ItemData, dataExists: boolean): PObject {
        const obj = new c(this.editAreaContainer, this.itemClickHandler, data, dataExists) as TYPE
        return obj;
    }

    // Resize the renderer, e.g. when window size changes
    public resize(): void {        
        //console.log(`PEditArea resized to ${this.canvas.clientWidth},${this.canvas.clientHeight}`);
        this.pixiApp.renderer.resize(this.canvas.clientWidth / this.zoom, this.canvas.clientHeight / this.zoom);
    }

    public zoomOut(): void   { if (this.zoom < 3.5) { this.zoom += 0.5; this.resize(); } }
    public zoomIn(): void    { if (this.zoom > 0.5) { this.zoom -= 0.5; this.resize(); }  }
    public zoomReset(): void { this.zoom = 1.0; this.resize(); }

    private align(v: number): number {
        if (projectData.snapToGrid()) {
            const GRID = projectData.snapGridSize();
            return v - (v % GRID);
        }
        return Math.round(v);
    }

    /*
      *   event handles, arrow syntax
      */

    // some subitem has been clicked, we need this as stopPropagation()
    // source = clicked pixi object
    // down: mouse down/up?
    // type: -1=item itself, otherwise drag corner 0-7 (0=NW, cw)
    private itemClickHandler = (source: PObject, down: boolean, type: number): void => {
        //console.log("SubitemClick " + down);
        if (type < 0) {
            if (down) {
                if (source.isSelected()) {
                    // nothing here - keep current selections
                }
                else {
                    viewData.items.forEach(i => i.graphics?.select(false));
                    source.select(true);
                }
            }
            this.itemIsSelected = down;
        }
        else {
            //console.log("start resizing " + type.toString());
            this.resizing = type;
        }        
    }

    private onDragStart = (event: PIXI.interaction.InteractionEvent): void => {
        if (this.resizing >= 0) {
            //console.log("resize");
            this.moveX0 = event.data.global.x;
            this.moveY0 = event.data.global.y;
        }
        else if (!this.itemIsSelected) {  // click on bgnd
            //console.log("desel");
            viewData.items.forEach(i => i.graphics?.select(false));
            this.lasso.start(event.data.global.x, event.data.global.y);
        }
        else {
            //console.log("move");
            this.moveX0 = this.align(event.data.global.x);
            this.moveY0 = this.align(event.data.global.y);
            this.dragMoving = true;
        }
        this.draw();
    }

    private onDragEnd = (event: PIXI.interaction.InteractionEvent): void => {
        if (this.resizing >= 0) {
            //console.log("RESIZE END");
            this.resizing = -1;
            viewData.pushUndoIfChanged();
        }
        else if (this.dragMoving) {
            //console.log("MOVE END");
            this.dragMoving = false;
            viewData.pushUndoIfChanged();
        }
        else if (!this.itemIsSelected) {  // lassoing
            //console.log("LASSO END");
            if (this.lasso.on) {
                this.lasso.end();
            }
        }
        this.draw();
    }
    
    private onDragMove = (event: PIXI.interaction.InteractionEvent): void => {
        if (!viewData.running) {  // no dragging when running
            if (this.resizing >= 0) {
                viewData.items.forEach((item) => { 
                    if (item.selected) {
                        const x = this.align(event.data.global.x);
                        const y = this.align(event.data.global.y);
                        item.graphics?.resize(this.resizing, this.align(x - this.moveX0), this.align(y - this.moveY0));
                    }
                });
                this.moveX0 = this.align(event.data.global.x);
                this.moveY0 = this.align(event.data.global.y);
            }
            else if (!this.itemIsSelected) {
                if (this.lasso.on) {
                    this.lasso.move(event.data.global.x, event.data.global.y);
                    //console.log("LMOVE");
                    viewData.items.forEach(i => i.graphics?.selectIfInside(this.lasso));
                }
            }
            else if (this.dragMoving) {
                const x = this.align(event.data.global.x);
                const y = this.align(event.data.global.y);
                viewData.items.forEach((item) => { 
                    if (item.selected)
                        item.graphics?.move(x - this.moveX0, y - this.moveY0);
                });
                this.moveX0 = this.align(event.data.global.x);
                this.moveY0 = this.align(event.data.global.y);
            }
            this.draw();
        }
    }
}
