import * as PIXI from 'pixi.js'

export default class {
    public on = false;

    private pixiContainer: PIXI.Container;
    private lasso: PIXI.Graphics;
    private x0 = 0;
    private y0 = 0;
    private x1 = 0;  // note: can be smaller than x0!
    private y1 = 0;

    public constructor(pixi: PIXI.Container) {
        this.pixiContainer = pixi;
        this.lasso = new PIXI.Graphics();
        this.lasso.zIndex = 9999999;
        this.pixiContainer.addChild(this.lasso);
    }

    public start(x: number, y: number): void {
        this.x0 = x;
        this.y0 = y;
        this.on = true;
    }

    public move(x: number, y: number): void {
        this.lasso.clear();
        this.lasso.lineStyle(1, 0x090909, 1);
        this.lasso.beginFill(0xeeeeee, 0.5);
        const w = x - this.x0;
        const h = y - this.y0;
        this.lasso.drawRect(this.x0, this.y0, w, h);
        this.x1 = this.x0 + w;
        this.y1 = this.y0 + h;
        this.lasso.endFill();    
    }

    public end(): void {
        this.lasso.clear();
        this.on = false;
    }

    public isRectInside(x: number, y: number, w: number, h: number): boolean {
        let x0: number;
        let y0: number;
        let x1: number;
        let y1: number;
        
        if (this.x0 < this.x1) {
            x0 = this.x0;
            x1 = this.x1;
        } 
        else {
            x0 = this.x1;
            x1 = this.x0;
        }
        if (this.y0 < this.y1) {
            y0 = this.y0;
            y1 = this.y1;
        } 
        else {
            y0 = this.y1;
            y1 = this.y0;
        }

        //console.log(`item ${x},${y} sz ${w},${h}`);
        //console.log(`lasso ${x0},${y0},  ${x1},${y1}`);

        if (x >= x0 && x+w <= x1 && y >= y0 && y+h <= y1)
            return true;
        return false;
    }
}