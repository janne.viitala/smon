/* Enum display; text/color based on value */

import * as PIXI from 'pixi.js'
import PObject from './PObject';
import AbstractLabel from './PAbstractLabel';
import { ItemData, PropertyGroup, Property, PropertyType, Enum, Color } from '../ItemData';
import { signalSource } from '../SignalSource';
import PAbstractLabel from './PAbstractLabel';

enum PropGroupIndex {
    ENUMPROPS = 1
}

// order must match push order in c'tor
enum EnumPropIndex {
    SIGNAL=0,
    ENUMS
}


export default class extends AbstractLabel {
    private _value = 0;
    private _sh = 0;  // signal handle
    private _run = false;
    private _current: Enum;

    public constructor(cont: PIXI.Container, cb: (src: PObject, down: boolean, type: number) => void, data: ItemData, dataExists: boolean) {
        super(cont, cb, data, dataExists);
        if (!dataExists) {
            // create our properties
            const pg: PropertyGroup = this.data.properties[PropGroupIndex.ENUMPROPS];
            pg.name = "Enumeration";
            pg.props.push(new Property("Signal", PropertyType.SIGNAL, ""));
            const e1 = new Enum("Value A", 0);
            const e2 = new Enum("Value B", 1);
            const enums = [ e1, e2 ]
            pg.props.push(new Property("Enumerations", PropertyType.ENUM, enums));

            this.setBgColorRGB("#000055");
            this.setTxtColorRGB("#ffffff");
        }
        this._current = this.getCurrentEnum();
        this.drawIt();
    }

    public toggleRunMode(run: boolean): void {
        // by default nothing
        this._run = run;
        if (run) {
            this._sh = signalSource.order(this.signal());
        }
        else {
            signalSource.cancelOrder(this.signal());
        }
    }   

    // override
    protected bgColor(): Color {
        if (this._current?.customColors) {
            return this._current.backgroundColor;
        }
        return super.bgColor();
    }


    // override
    protected  txtColor(): Color {
        if (this._current?.customColors) {
            return this._current.textColor;
        }
        return super.txtColor();
    }

    private getCurrentEnum(): Enum {
        this._value = signalSource.value(this._sh);
        const enums = this.data.properties[PropGroupIndex.ENUMPROPS].props[EnumPropIndex.ENUMS].value;
        for (let i=0; i<enums.length; i++) {
            if (enums[i].value === this._value) {
                return enums[i];
            }
        }
        return new Enum(this._value.toString() + " undefined", 0);
    }

    private updateEnum(): void {
        this._current = this.getCurrentEnum();
        this._text = this._current?.label;
    }

    public updateValues(): void {
        this.updateEnum();
        this.updateText();
        super.drawIt();
    }

    private signal(): string { return this.data.properties[PropGroupIndex.ENUMPROPS].props[EnumPropIndex.SIGNAL].value; }

    protected drawIt(): void {
        if (this._run) {
            this.updateEnum();
        }
        super.drawIt();        
    }

}
