/* Number display, dec/hex, etc cool stuff */

import * as PIXI from 'pixi.js'
import PObject from './PObject';
import AbstractLabel from './PAbstractLabel';
import { ItemData, PropertyGroup, Property, PropertyType } from '../ItemData';
import { signalSource } from '../SignalSource';

enum PropGroupIndex {
    NUMBERPROPS = 1
}

const DEC="Decimal";
const HEX="Hex";

// order must match push order in c'tor
enum NumberPropIndex {
    SIGNAL=0,
    OFFSET,
    SCALE,
    MODE,
    DECIMALS
}


export default class extends AbstractLabel {
    private _value = 123.4;
    private _sh = 0;  // signal handle

    public constructor(cont: PIXI.Container, cb: (src: PObject, down: boolean, type: number) => void, data: ItemData, dataExists: boolean) {
        super(cont, cb, data, dataExists);
        if (!dataExists) {
            // create our properties
            const pg: PropertyGroup = this.data.properties[PropGroupIndex.NUMBERPROPS];
            pg.name = "Number";
            pg.props.push(new Property("Signal", PropertyType.SIGNAL, ""));
            pg.props.push(new Property("Offset", PropertyType.NUMBER, 0));
            pg.props.push(new Property("Scale", PropertyType.NUMBER, 1));
            pg.props.push(new Property("Mode", PropertyType.SELECTION, { sel: DEC, options: [DEC, HEX ] }));
            pg.props.push(new Property("Decimals", PropertyType.NUMBER, 0));

            this.setBgColorRGB("#0071BC");
            this.setTxtColorRGB("#ffffff");
            this.setFontFixed(true);
        }
        this.drawIt();
    }

    public toggleRunMode(run: boolean): void {
        // by default nothing
        if (run) {
            this._sh = signalSource.order(this.signal());
        }
        else {
            signalSource.cancelOrder(this.signal());
        }
    }    

    public updateValues(): void {
        this._value = signalSource.value(this._sh);
        this.formatValueText();
        this.updateText();
    }

    private formatValueText(): void {
        const value = (this._value - this.offset()) * this.scale();
        if (this.mode() == HEX) {
            const zeros = "0000000000000000";  // 64 bits ought to be enough for everyone
            const str = Math.floor(value).toString(16).toUpperCase();
            if (this.decimals() > str.length) {
                this._text = (zeros + str).substr(-this.decimals());
            }
            else {
                this._text = str;
            }
            this._text = "#" + this._text;
        }
        else {  // dec
            this._text = value.toFixed(this.decimals());
        }
    }

    private signal(): string { return this.data.properties[PropGroupIndex.NUMBERPROPS].props[NumberPropIndex.SIGNAL].value; }
    private offset(): number { return this.data.properties[PropGroupIndex.NUMBERPROPS].props[NumberPropIndex.OFFSET].value; }
    private scale(): number { return this.data.properties[PropGroupIndex.NUMBERPROPS].props[NumberPropIndex.SCALE].value; }
    private mode(): string { return this.data.properties[PropGroupIndex.NUMBERPROPS].props[NumberPropIndex.MODE].value.sel; }
    private decimals(): number { return this.data.properties[PropGroupIndex.NUMBERPROPS].props[NumberPropIndex.DECIMALS].value; }

    protected drawIt(): void {
        this.formatValueText();
        super.drawIt();        
    }

    public writeValue(value: number): void {
        signalSource.write(this._sh, value);
    }
}
