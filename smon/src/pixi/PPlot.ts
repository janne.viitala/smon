/* Plot widget */

import * as PIXI from 'pixi.js'
import PObject from './PObject';
import { ItemData, PropertyGroup, Property, PropertyType, Color } from '../ItemData';
import RingBuffer from '../RingBuffer';

const MAX_LINES = 8;
const MAX_LABELS = 11;

enum PropGroupIndex {    
    TRENDPROPS = 1,
    APPEARANCE,
    LINEPROPS0 // lines 1-7 follow
}

// order must match push order in c'tor
enum TrendPropIndex {
    YMAX = 0,
    YMIN,
    BUFFERLEN,
    SWEEP,
    GRIDY,
    GRIDX,
    SHOWREFLEVEL,
    REFLEVEL
}

enum ApprearanceProperties {
    GRIDCOLOR=0,
    ANGLE
}

enum LinePropIndex {
    SIGNAME=0,
    LABEL,
    COLOR,
    OFFSET,
    SCALE
}

const TOP_MARGIN = 40;
const MARGIN = 10;

class Sample {
    public ms: number;
    public value: number;
    public constructor(value: number, ms: number) {
        this.value = value;
        this.ms = ms;
    }
}


const LINEDEFAULTCOLORS = ['#FF1493', '#FFA500', '#6A5ACD', '#32CD32', '#FF4500', '#A52A2A', '#FF00FF', '#FFD700' ];

export default class extends PObject {
    private bgndBuffer: PIXI.Graphics;
    private trends: PIXI.Graphics;
    private yLabels: Array<PIXI.Text>;
    private xDivText: PIXI.Text;
    private labelStyle: PIXI.TextStyle;
    private leftMargin = 50;
    private samples: Array<RingBuffer<Sample>> = [];
    // cursor:
    private showCursor = true;
    private cursorX = 0;
    private cursorY = 0;
    private dragX = 0;
    private dragY = 0;

    public constructor(cont: PIXI.Container, cb: (src: PObject, down: boolean, type: number) => void, data: ItemData, dataExists: boolean) {
        super(cont, cb, data, dataExists);

        //console.log(`creating plot to ${this.data.x()}, ${this.data.y()}`);
        let pg = new PropertyGroup;
        pg.name = "Plot Settings";                
        pg.props.push(new Property("Y-max", PropertyType.NUMBER, 100));
        pg.props.push(new Property("Y-min", PropertyType.NUMBER, 0));
        pg.props.push(new Property("Buffer Length [samples]", PropertyType.NUMBER, 10000));
        pg.props.push(new Property("Sweep [pixel/s]", PropertyType.NUMBER, 100));
        pg.props.push(new Property("Y Grid Step", PropertyType.NUMBER, 10));
        pg.props.push(new Property("X Grid Step [s]", PropertyType.NUMBER, 1));
        pg.props.push(new Property("Show Reference Level", PropertyType.BOOLEAN, false));
        pg.props.push(new Property("Reference Level", PropertyType.NUMBER, 0));
        pg.props.push(new Property("Rotation angle (use with care)", PropertyType.NUMBER, 0));        
        this.data.properties.push(pg);

        pg = new PropertyGroup;
        pg.name = "Appearance";
        pg.props.push(new Property("Grid Color", PropertyType.COLOR, { rgb: "#222222", a: 1 }));
        pg.props.push(new Property("Rotation angle (use with care)", PropertyType.NUMBER, 0));        
        this.data.properties.push(pg);

        // signals
        for (let sigNr=1; sigNr<=MAX_LINES; sigNr++) {
            pg = new PropertyGroup;
            pg.name = "Signal " + sigNr.toString();
            pg.props.push(new Property("Signal Name", PropertyType.STRING, ''));
            pg.props.push(new Property("Line Label", PropertyType.STRING, ''));
            pg.props.push(new Property("Line Color", PropertyType.COLOR, { rgb: LINEDEFAULTCOLORS[sigNr-1], a: 1 }));
            pg.props.push(new Property("Offset", PropertyType.NUMBER, 0));
            pg.props.push(new Property("Scale", PropertyType.NUMBER, 1));
            this.data.properties.push(pg);
        }
        
        this.setw(300);
        this.seth(200);

        for (let i = 0; i < MAX_LINES; i++) {
            this.samples.push(new RingBuffer<Sample>(10000));
        }

        // create PIXI stuff
        this.bgndBuffer = new PIXI.Graphics();
        this.trends = new PIXI.Graphics();
        this.trends.x = 0;
        this.trends.y = 0;
        this.gElem.addChild(this.bgndBuffer);
        this.bgndBuffer.addChild(this.trends);
        this.yLabels = [];
        this.labelStyle = new PIXI.TextStyle({fontFamily : 'Arial', fontSize: 12, fill : 0xffffff, align : 'right'});
        for (let i=0; i<MAX_LABELS; i++) {
            const txt = new PIXI.Text("", this.labelStyle)
            this.yLabels.push(txt);
            this.bgndBuffer.addChild(txt);
        }
        this.xDivText = new PIXI.Text("", this.labelStyle)
        this.gElem.addChild(this.xDivText);
        this.drawIt();
    }


    private yMax(): number { return this.data.properties[PropGroupIndex.TRENDPROPS].props[TrendPropIndex.YMAX].value; }
    private yMin(): number {return this.data.properties[PropGroupIndex.TRENDPROPS].props[TrendPropIndex.YMIN].value; }
    private bufferLen(): number {return this.data.properties[PropGroupIndex.TRENDPROPS].props[TrendPropIndex.BUFFERLEN].value; }
    private sweep_px_s(): number {return this.data.properties[PropGroupIndex.TRENDPROPS].props[TrendPropIndex.SWEEP].value; }
    private yGrid(): number {return this.data.properties[PropGroupIndex.TRENDPROPS].props[TrendPropIndex.GRIDY].value; }
    private xGrid(): number {return this.data.properties[PropGroupIndex.TRENDPROPS].props[TrendPropIndex.GRIDX].value; }
    private showRefLevel(): boolean {return this.data.properties[PropGroupIndex.TRENDPROPS].props[TrendPropIndex.SHOWREFLEVEL].value; }
    private refLevel(): number {return this.data.properties[PropGroupIndex.TRENDPROPS].props[TrendPropIndex.REFLEVEL].value; }
    // lines, line nr 0..7
    private signalName(nr: number): string { return this.data.properties[PropGroupIndex.LINEPROPS0 + nr].props[LinePropIndex.SIGNAME].value; }
    private lineColor(nr: number): Color { return this.data.properties[PropGroupIndex.LINEPROPS0 + nr].props[LinePropIndex.COLOR].value; }
    private offset(nr: number): number { return this.data.properties[PropGroupIndex.LINEPROPS0 + nr].props[LinePropIndex.OFFSET].value; }
    private scale(nr: number): number { return this.data.properties[PropGroupIndex.LINEPROPS0 + nr].props[LinePropIndex.SCALE].value; }
    private gridColor(): Color { return this.data.properties[PropGroupIndex.APPEARANCE].props[ApprearanceProperties.GRIDCOLOR].value; }
    private angle(): number { return this.data.properties[PropGroupIndex.APPEARANCE].props[ApprearanceProperties.ANGLE].value; }


    // return y coordinate of given value
    private scaleYValue(v: number): number {
        const yMin = 0;
        const yMax = this.data.h() - (TOP_MARGIN + MARGIN);        
        const yRange = yMax;
        const vMax = this.yMax();  // note: v grows upwards
        const vMin = this.yMin();
        const vRange = vMax - vMin;
        let y: number;
        if (v <= vMin) {
            y = yMax;
        }
        else if (v >= vMax) {
            y = yMin;
        }
        else {
            y = yMin + (yRange - (((v - vMin) / vRange) * yRange));
        }
        return y;
    }

    // place up to MAX_LABELS text labels to horizontal grid lines
    private arrangeYLabels(): void {
        const lines = ((this.yMax() - this.yMin()) / this.yGrid()); // +1 for 1st line
        let labelLineStep = 1;
        while ((lines / labelLineStep) > MAX_LABELS) {            
            labelLineStep++;
        }
        let i: number;
        for (i=0; i<lines/labelLineStep; i++) {
            this.yLabels[i].visible = true;
            const v = this.yMin() + i*this.yGrid()*labelLineStep;
            this.yLabels[i].text = v.toString();
            this.yLabels[i].x = -(this.yLabels[i].width + 5);
            this.yLabels[i].y = this.scaleYValue(v) - 8;
        }    
        // hide rest    
        for (; i<MAX_LABELS; i++) {
            this.yLabels[i].visible = false;
        }
        // show top edge value always
        this.yLabels[MAX_LABELS - 1].visible = true;
        this.yLabels[MAX_LABELS - 1].text = this.yMax().toString();
        this.yLabels[MAX_LABELS - 1].x = -(this.yLabels[MAX_LABELS - 1].width + 5);
        this.yLabels[MAX_LABELS - 1].y = this.scaleYValue(this.yMax()) - 8;
        
    }

    // "buffer", as this does not change when plotting
    private drawBgndBuffer(): void {        
        // figure out proper left marigin, just enough to hold y-labels
        const maxLabelLen = PIXI.TextMetrics.measureText(this.yMax().toString(), this.labelStyle).width;
        const minLabelLen = PIXI.TextMetrics.measureText(this.yMin().toString(), this.labelStyle).width;
        this.leftMargin = Math.max(maxLabelLen, minLabelLen) + 10;
        //console.log(`left marigin is ${this.leftMargin} px`);

        this.bgndBuffer.x = this.leftMargin;
        this.bgndBuffer.y = TOP_MARGIN;
        const w = this.data.w() - (this.leftMargin + MARGIN);
        const h = this.data.h() - (TOP_MARGIN + MARGIN);        
        this.bgndBuffer.clear();
        // bgnd
        this.bgndBuffer.beginFill(0x000000, 1);
        this.bgndBuffer.drawRect(0, 0, w, h);
        // grid
        const Y_GRID_STEP = (this.yGrid() / (this.yMax() - this.yMin())) * h;
        let xGridStep = this.xGrid() * this.sweep_px_s();
        if (xGridStep <= 0) {
            //console.log(`limiting too small x grid step ${xGridStep} to 1`);
            xGridStep = 1;
        }      
        const c: Color = this.gridColor();
        this.bgndBuffer.lineStyle(1, PIXI.utils.string2hex(c.rgb), c.a);
        let x: number, y: number;
        for (x=w; x>=0; x-=xGridStep) {
            this.bgndBuffer.moveTo(x, 0);
            this.bgndBuffer.lineTo(x, h);
        }
        for (y=h; y>=0; y-=Y_GRID_STEP) {
            this.bgndBuffer.moveTo(0, y);
            this.bgndBuffer.lineTo(w, y);
        }
        // optional reference level
        if (this.showRefLevel()) {
            this.bgndBuffer.lineStyle(1, 0xff0000, 1);
            const y = this.scaleYValue(this.refLevel());
            this.bgndBuffer.moveTo(0, y);
            this.bgndBuffer.lineTo(w, y);
        }
        // x div label
        this.xDivText.y = 10;
        this.xDivText.x = this.data.w() - 50;
        this.xDivText.text = "X: " + this.xGrid().toString() + "s";
        this.arrangeYLabels();
    }


    private getSample(ch: number): Sample {
        let s: Sample;
        if (ch == 0)
            s = new Sample(Math.sin(performance.now() / 500) * 100, performance.now());
        else if (ch === 1)
            s = new Sample(Math.cos(performance.now() / 400) * 100, performance.now());
        else 
            s = new Sample(Math.random() * 100, performance.now());
        return s;
    }


    // read value(s) & draw content
    public updateValues(): void {
        this.samples[0].put(this.getSample(0));
        this.samples[1].put(this.getSample(1));
        this.samples[2].put(this.getSample(2));
        this.drawTrends();
    }

    private drawTrends(): void {
        const current_ms = performance.now();

        //console.time("LINES");
        const w = this.data.w() - (this.leftMargin + MARGIN);
        const h = this.data.h() - (TOP_MARGIN + MARGIN);        
        this.trends.clear();
        let sampleIndex: number;
        const MAX_SAMPLES = this.bufferLen();
        for (let lineNr = 0; lineNr < MAX_LINES; lineNr++) {
            if (this.signalName(lineNr).length > 0) {
                const c = this.lineColor(lineNr);
                this.trends.lineStyle(1, PIXI.utils.string2hex(c.rgb), c.a);
                const s0 = this.samples[lineNr].peek(0);
                if (s0 !== undefined) {
                    this.trends.moveTo(w, this.scaleYValue((s0.value + this.offset(lineNr)) * this.scale(lineNr))); 
                    for (sampleIndex = 1; sampleIndex < MAX_SAMPLES; sampleIndex++) {
                        const sample: Sample = this.samples[lineNr].peek(sampleIndex);
                        if (sample === undefined) { // out of samples
                            break;
                        }
                        const v = (sample.value + this.offset(lineNr)) * this.scale(lineNr);
                        const y = this.scaleYValue(v);
                        const x = ((current_ms - sample.ms) * this.sweep_px_s()) / 1000;
                        if (x >= w)            // reached end of the visible area
                            break;
                        this.trends.lineTo(w - x, y);
                    }
                }
            }
        }
        //console.log(`draw line of ${sampleIndex} samples`);
        //console.timeEnd("LINES");  // 1-3ms in i5-8400 with 1100px plot! 
    }

    private mouseMove (ev): void {
        // optinal cursor
        //this.cursorX = ev.clientX - this.$refs.gc.getBoundingClientRect().left;
        //this.cursorY = ev.clientY - this.$refs.gc.getBoundingClientRect().top;;
        // select something
    }
    private mouseLeave (ev): void {
        //this.cursorX = -1;
        //this.dragging = false;
    }
    private mouseDown (ev): void {
        //this.dragX = ev.clientX - this.$refs.gc.getBoundingClientRect().left;;
        //this.dragY = ev.clientY - this.$refs.gc.getBoundingClientRect().top;;
    }

    protected drawIt(): void {
        //console.log(`draw plot size ${this.gElem.width}, ${this.gElem.height}, data says ${this.data.w()},${this.data.h()}`)
        super.drawIt();  // calls gElem.clear();
        this.gElem.beginFill(0x0c0c0c, 1);
        this.gElem.drawRect(0, 0, this.data.w(), this.data.h());
        this.drawBgndBuffer();
        this.drawTrends();
        this.gElem.angle = this.angle();
    }
}
