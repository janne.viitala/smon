/**
 *  Pixi based object base class - provides gui editing functions (move, resize, ...) together 
 *  with PEditArea, fancy stuff is in properties that can be edited in property editor.
 *  
 *  Provides gElem as base PIXI.Graphics to draw on for derived classes.
 * 
 *  Note about PIXI.Graphics: *do not* use (read or especially write) it's width/height property, 
 *  as you will just mess up scaling. You can use it's x/y, and then use this.w() etc to draw
 *  stuff correctly.
 * 
 */

import * as PIXI from 'pixi.js'
import { ItemData, IGraphicsObject } from '../ItemData';
import { viewData } from '../ViewData';
import Lasso from './PLasso';

class DragBox {
    public sprite: PIXI.Sprite;
    public corner: number;

    public constructor (corner: number) {
        this.corner = corner;
        const texture = PIXI.Texture.from('./dragbox14.png');
        this.sprite = PIXI.Sprite.from(texture);
        this.sprite.interactive = true;
        //dbox.anchor.set(0.5)
    }
}

export default class PObject implements IGraphicsObject {
    protected gElem: PIXI.Graphics;
    protected data: ItemData;

    private clickCb: (source: PObject, down: boolean, type: number) => void;
    private pixiContainer: PIXI.Container;
    private dragging = false;
    private visible = true;
    private selBox: Array<DragBox>;
    private runSelBox: PIXI.Graphics;  // run mode selection border


    /** 
     * c'tor:
     * p: PIXI container where object is added to
     * cb: callback for mouse handling
     * data: ItemData structure than contains all the persistent data
     * dataExists: does all data pre-exists? if not then PObject derivative must add it's own data to 'data'.
     *   NOTE: when creating new object data does not exist yet, when copy/pasting it does. 
     */ 
    public constructor(p: PIXI.Container, cb: (src: PObject, down: boolean, type: number) => void, data: ItemData, dataExists: boolean) {
        this.pixiContainer = p;
        this.gElem = new PIXI.Graphics();
        this.gElem.interactive = true;
        this.clickCb = cb;
        this.data = data;
        data.attachPObject(this); // link json data object to our fancy graphics

        //this.data.updateCb = this.drawIt;
        // hook drag events
        this.gElem.on('mousedown', this.onDragStart)  
                  .on('mouseup',  this.onDragEnd)
                  .on('mouseupoutside',  this.onDragEnd)
                  .on('mousemove', this.onDragMove);
        this.pixiContainer.addChild(this.gElem);     
        this.setz(viewData.largestZIndex()+1);
        // create resize-box sprites
        this.selBox = [];  
        for (let i=0; i<8; i++) {
            //console.log(dbox);
            const db = new DragBox(i);
            this.selBox.push(db);
            this.gElem.addChild(db.sprite);
        }
        this.selBox[0].sprite.on('mousedown', this.selBox0Click)
        this.selBox[1].sprite.on('mousedown', this.selBox1Click)
        this.selBox[2].sprite.on('mousedown', this.selBox2Click)
        this.selBox[3].sprite.on('mousedown', this.selBox3Click)
        this.selBox[4].sprite.on('mousedown', this.selBox4Click)
        this.selBox[5].sprite.on('mousedown', this.selBox5Click)
        this.selBox[6].sprite.on('mousedown', this.selBox6Click)
        this.selBox[7].sprite.on('mousedown', this.selBox7Click)
        this.runSelBox = new PIXI.Graphics();
        this.runSelBox.x = 0;
        this.runSelBox.y = 0;
        this.gElem.addChild(this.runSelBox);
    }


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     *    PObjectInterface
     */

    // update request from reactive world
    public update = (): void => {         
        this.drawIt();
        if (!this.visible) {
            this.unDelete();
        }
    }

    public isSelected(): boolean {
        return this.data.selected;
    }

    public select(s: boolean): void {
        if (s != this.data.selected) {
            this.data.selected = s;
            // skip full drawIt() for performance
            if (viewData.running) {
                this.runSelBox.visible = s;
                this.selBox.forEach((sb) => { sb.sprite.visible = false; }) 
            }
            else {
                this.runSelBox.visible = false;
                this.selBox.forEach((sb) => { sb.sprite.visible = s; }) 
            }
        }
    }

    public selectIfInside(lasso: Lasso): void {
        this.select(lasso.isRectInside(this.x(), this.y(), this.w(), this.h()));
    }

    public move(dx: number, dy: number): void {
        this.setx(this.x() + dx);
        this.sety(this.y() + dy);
        this.gElem.x = this.x();
        this.gElem.y = this.y();       
    }

    // lazy bum's undo/redo changes the whole data
    public setData = (data: ItemData): void => {
        this.data = data;           
        this.data.selected = false; // lose selection when undo'ing or redo'ing
        if (!this.visible) {
            this.unDelete();
        }
        this.drawIt();
     }

    public updateValues(): void {
        // nothing here, derived classes use this
     }  

    public delete(): void {
        this.pixiContainer.removeChild(this.gElem);
        this.visible = false;
    }

    private unDelete(): void {
        this.pixiContainer.addChild(this.gElem);
        this.visible = true;
    }

    public resize(corner: number, dx: number, dy: number): void {
        const MIN_SIZE = 10;
        //console.log("sz " + dx.toString() + ", " + dy.toString());
        if (corner === 0) {
            const newW = this.w() - dx;
            if (newW >= MIN_SIZE) {
                this.setx(this.x() + dx);
                this.setw(newW);
            }
            const newH = this.h() - dy;
            if (newH >= MIN_SIZE) {
                this.sety(this.y() + dy);
                this.seth(newH);
            }
        }
        else if (corner === 1) {
            const newH = this.h() - dy;
            if (newH >= MIN_SIZE) {
                this.sety(this.y() + dy);
                this.seth(newH);
            }
        }
        else if (corner === 2) {
            const newW = this.w() + dx;
            if (newW >= MIN_SIZE) {
                this.setw(newW);
            }
            const newH = this.h() - dy;
            if (newH >= MIN_SIZE) {
                this.sety(this.y() + dy);
                this.seth(newH);
            }
        }
        else if (corner === 3) {
            const newW = this.w() + dx;
            if (newW >= MIN_SIZE) {
                this.setw(newW);
            }
        }
        else if (corner === 4) {
            const newW = this.w() + dx;
            if (newW >= MIN_SIZE) {
                this.setw(newW);
            }
            const newH = this.h() + dy;
            if (newH >= MIN_SIZE) {
                this.seth(newH);
            }
        }
        else if (corner === 5) {
            const newH = this.h() + dy;
            if (newH >= MIN_SIZE) {
                this.seth(newH);
            }
        }
        else if (corner === 6) {
            const newW = this.w() - dx;
            if (newW >= MIN_SIZE) {
                this.setx(this.x() + dx);
                this.setw(newW);
            }
            const newH = this.h() + dy;
            if (newH >= MIN_SIZE) {
                this.seth(newH);
            }
        }
        else if (corner === 7) {
            const newW = this.w() - dx;
            if (newW >= MIN_SIZE) {
                this.setx(this.x() + dx);
                this.setw(newW);
            }
        }
        this.drawIt();  // need full re-draw as layout changed
    }

    public toggleRunMode(run: boolean): void {
        // by default nothing
    }

    // default write does nothing, objects actually supporting writing overload this
    public writeValue(value: number): void {
        console.log("object does not support writing");
    }


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     *    protected
     */

    // getters/setters for data living viewData; shortcuts to ItemData functions
    protected x(): number { return this.data.x(); }
    protected y(): number { return this.data.y(); }
    protected w(): number { return this.data.w(); }
    protected h(): number { return this.data.h(); }
    protected z(): number { return this.data.z(); }
    protected setx(v: number): void { this.data.setx(v); }
    protected sety(v: number): void { this.data.sety(v); }
    protected setw(v: number): void { this.data.setw(v); }
    protected seth(v: number): void { this.data.seth(v); }
    protected setz(v: number): void { this.data.setz(v); }

    protected drawIt(): void {
        this.gElem.clear();
        this.gElem.x = this.x();
        this.gElem.y = this.y();
        this.gElem.zIndex = this.z();        
        if (this.data.selected) {  
            if (viewData.running) {    // run mode - sel border
                for (let i=0; i<8; i++) {
                    this.selBox[i].sprite.visible = false;
                }
                this.runSelBox.visible = true;
                this.runSelBox.clear();
                this.runSelBox.lineStyle(4, 0x3333ff, 0.5);
                this.runSelBox.drawRect(-4, -4, this.w() + 8, this.h() + 8);
            }
            else {                     // edit mode - position resize knobs                
                const BOXSIZE = 14;    // FIXME magic number...
                this.selBox[0].sprite.x = -BOXSIZE/2;
                this.selBox[0].sprite.y = -BOXSIZE/2;
                this.selBox[1].sprite.x = +this.w()/2-BOXSIZE/2;
                this.selBox[1].sprite.y = this.selBox[0].sprite.y;
                this.selBox[2].sprite.x = this.w()-BOXSIZE/2;
                this.selBox[2].sprite.y = this.selBox[0].sprite.y;
                this.selBox[3].sprite.x = this.selBox[2].sprite.x;
                this.selBox[3].sprite.y = this.h()/2-BOXSIZE/2;
                this.selBox[4].sprite.x = this.selBox[2].sprite.x;
                this.selBox[4].sprite.y = this.h()-BOXSIZE/2;
                this.selBox[5].sprite.x = this.selBox[1].sprite.x
                this.selBox[5].sprite.y = this.selBox[4].sprite.y;
                this.selBox[6].sprite.x = this.selBox[0].sprite.x
                this.selBox[6].sprite.y = this.h()-BOXSIZE/2;
                this.selBox[7].sprite.x = this.selBox[0].sprite.x
                this.selBox[7].sprite.y = this.selBox[3].sprite.y
                for (let i=0; i<8; i++) {
                    this.selBox[i].sprite.visible = true;
                }
                this.runSelBox.visible = false;
                this.runSelBox.clear();
            }
        }
        else {
            this.runSelBox.visible = false;
            this.runSelBox.clear();
            for (let i=0; i<8; i++) {
                this.selBox[i].sprite.visible = false;
            }
        }
    }
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     *    private
     */


    /*
     *  event handlers, arrow syntax needed
     */ 
    private onDragStart = (event: PIXI.interaction.InteractionEvent): void => {
        //console.log("onDragStart");
        event.stopPropagation();  // does not seem to stop event from flowing to Application
        this.dragging = true;
        this.clickCb(this, true, -1);
    }

    private onDragEnd = (event: PIXI.interaction.InteractionEvent): void => {
        if (this.dragging) {
            event.stopPropagation();
            this.dragging = false;
            this.clickCb(this, false, -1);
            this.drawIt();
        }
    }

    private onDragMove = (event: PIXI.interaction.InteractionEvent): void => {        
        if (this.dragging) {
            event.stopPropagation();
        }
    }

    private selBox0Click = (event: PIXI.interaction.InteractionEvent): void => {
        event.stopPropagation(); 
        this.clickCb(this, true, 0);
    }
    private selBox1Click = (event: PIXI.interaction.InteractionEvent): void => {
        event.stopPropagation(); 
        this.clickCb(this, true, 1);
    }
    private selBox2Click = (event: PIXI.interaction.InteractionEvent): void => {
        event.stopPropagation(); 
        this.clickCb(this, true, 2);
    }
    private selBox3Click = (event: PIXI.interaction.InteractionEvent): void => {
        event.stopPropagation(); 
        this.clickCb(this, true, 3);
    }
    private selBox4Click = (event: PIXI.interaction.InteractionEvent): void => {
        event.stopPropagation(); 
        this.clickCb(this, true, 4);
    }
    private selBox5Click = (event: PIXI.interaction.InteractionEvent): void => {
        event.stopPropagation(); 
        this.clickCb(this, true, 5);
    }
    private selBox6Click = (event: PIXI.interaction.InteractionEvent): void => {
        event.stopPropagation(); 
        this.clickCb(this, true, 6);
    }
    private selBox7Click = (event: PIXI.interaction.InteractionEvent): void => {
        event.stopPropagation(); 
        this.clickCb(this, true, 7);
    }
}
