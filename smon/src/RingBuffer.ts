/** ring buffer for numbers 
 * 
 *  TODO: if needed optimize read ptr away, as we (=plot) only use writing
 */

export default class RingBuffer<T> {
    private memory: Array<T>;
    private writeIndex: number;
    private readIndex: number;
    private isFull: boolean;

    public constructor(size: number) {    
      this.memory = new Array<T>(size);
      this.writeIndex = 0;
      this.readIndex = 0;
      this.isFull = false;
    }
    
    /** add new sample */
    public put(value: T): void {
      if (this.isFull) {
        console.log("Buffer full, dropping oldest");
        this.readIndex = this.next(this.readIndex);   // make some space
      }
      this.writeIndex = this.next(this.writeIndex);
      this.memory[this.writeIndex] = value;
      if (this.writeIndex === this.readIndex) {
        this.isFull = true;
      }
    }

    /** take (remove) oldest sample */
    public get(): T {
      if (this.readIndex === this.writeIndex && !this.isFull) {
        console.log('Nothing to read.');
        return undefined as unknown as T;   // wtf!?
      } 
      else {
        this.readIndex = this.next(this.readIndex);
        this.isFull = false;
        return this.memory[this.readIndex];
      }
    }  
   
    /** return (without removing) sample that is 'age' old */
    public peek(age: number): T {
      // sanity check:
      let ix: number;
      if (age < 0 || age >= this.memory.length) {
        console.log(`ringbuffer: trying to read older value than availale (requesting ${age}, buffer len is ${this.memory.length})`);
        return undefined as unknown as T;
      }
      else {
        let ix = this.writeIndex - age;
        if (ix < 0) {
          ix += this.memory.length;
        }
        return this.memory[ix];
      }
      // below stupid ts compiler gives error "Variable 'ix' is used before being assigned." even though it was...
      // return this.memory[ix];  
    }

    // advance ptr
    private next(n: number): number {
      const nxt = n + 1;
      if (nxt === this.memory.length) {
        return 0;
      }
      else {
        return nxt;
      }
    }
  }
  