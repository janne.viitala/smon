import Vue, { VNode } from 'vue'
import App from '@/App.vue'
import store from './store'
import vuetify from './plugins/vuetify';
import VueSplit from 'vue-split-panel'

Vue.config.productionTip = false

Vue.use(VueSplit)

new Vue({
  store,
  vuetify,
  render(h): VNode { return h(App); },
}).$mount('#app')
