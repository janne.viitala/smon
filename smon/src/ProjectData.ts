import Vue from 'vue';
import { PropertyGroup, PropertyType, Color } from './ItemData';

// notification mechanism for listeners that user (or something) has changed the data
export interface ProjectDataChangeListener {
  projectDataChanged: () => void;
}


export class ProjectData {
    private static _instance: ProjectData;
    private listeners: Array<ProjectDataChangeListener> = [];

    public properties: Array<PropertyGroup>;

    public static instance(): ProjectData {
      if (!this._instance) {
        this._instance = new this();
      }
      return this._instance;
    }

    public addListener(l: ProjectDataChangeListener): void {
      this.listeners.push(l);
    }

    public constructor() {
      this.properties = [{
        name: "Drawing Area",
        props: [{
            name: 'Background Color',
            type: PropertyType.COLOR,
            value: new Color("#9B9B9B", 1)
          }, {
            name: 'Show Area Border',
            type: PropertyType.BOOLEAN,
            value: true
          }, {
            name: 'Area Width',
            type: PropertyType.NUMBER,
            value: 1200
          }, {
            name: 'Area Height',
            type: PropertyType.NUMBER,
            value: 800
          }, {
            name: 'Snap To Grid',
            type: PropertyType.BOOLEAN,
            value: true
          }, {
            name: 'Snap Grid Size',
            type: PropertyType.NUMBER,
            value: 10
          }, {
            name: 'Show Visible Grid',
            type: PropertyType.BOOLEAN,
            value: false
          }, {
            name: 'Visible Grid Size',
            type: PropertyType.NUMBER,
            value: 20
          }, {
            name: 'Grid Color',
            type: PropertyType.COLOR,
            value: new Color("#6E6E6E", 1)
          }        
        ]
      }]    
    }  

    public bgColor(): Color          { return this.properties[0].props[0].value as Color; }
    public showAreaBorder(): boolean { return this.properties[0].props[1].value as boolean; }
    public areaWidth(): number       { return this.properties[0].props[2].value as number; }
    public areaHeight(): number      { return this.properties[0].props[3].value as number; }
    public snapToGrid(): boolean     { return this.properties[0].props[4].value as boolean; }
    public snapGridSize(): number    { return this.properties[0].props[5].value as number; }
    public showGrid(): boolean       { return this.properties[0].props[6].value as boolean; }
    public gridSize(): number        { return this.properties[0].props[7].value as number; }
    public gridColor(): Color        { return this.properties[0].props[8].value as Color; }

    // notify listeners about changed data
    public applyChanges(): void {
      this.listeners.forEach((l) => { l.projectDataChanged(); })
    }
}

// app signleton Data
export const projectData = Vue.observable(ProjectData.instance());

